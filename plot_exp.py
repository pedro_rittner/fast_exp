#!/usr/bin/env python3
"""
Simple plotting script for visualizing exponential data file.
"""
import numpy as np
from matplotlib import pyplot as plt
from sys import argv


def main(filename: str):
    """ Plot and display graph from data file. """
    data_arr = np.fromfile(filename)
    plt.plot(data_arr)
    plt.show()

if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: python plot_exp.py <name_of_data_file>")
        exit(0)
    main(argv[1])
