CC := gcc

# Add debugging flags only if asked
CFLAGS := -std=gnu99 -Wall -Wextra -Wpedantic
ifdef DEBUG
CFLAGS += -g -Og
else
CFLAGS += -O2 -march=native
endif

INCLUDES := -I. -I/usr/include
LPATH := -L/usr/lib
LDFLAGS := -lgsl -lgslcblas -lm
DEFINES ?= 
DEFINES += -D_GNU_SOURCE=1

.PHONY: all clean rebuild

rebuild: clean all

all: fast_exp.bin

fast_exp.bin:
	$(CC) $(CFLAGS) $(DEFINES) $(INCLUDES) $(LPATH) fast_exp.c -o $@ $(LDFLAGS)

clean:
	@rm -f *.bin
