#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include <string.h>

#include <math.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>

//! Implementation for Schraudolph 1999 approximation for the exponential.
__attribute__((used)) static union
{
    double d;
    struct
    {
#ifdef LITTLE_ENDIAN
        int32_t j, i;
#else
        int32_t i, j;
#endif
        
    } n;
} eco;
#define EXP_A 1512775
#define EXP_C 60801

//! Schraudolph 1999 approximation for exponential.
#define EXP_S99(x) (eco.n.i = EXP_A * x + (1072693248 - EXP_C), eco.d)

//! GNU math exponential
#define EXP_M(x) exp(x)

// Simulation parameters
#ifndef X_START
//! Starting lower bound for x values.
#define X_START 1.0
#endif
#ifndef X_END
//! Maximum upper bound for x values.
#define X_END 200.0
#endif
#ifndef DATA_ARR_LEN
//! Number of data points to collect.
#define DATA_ARR_LEN 1000000
#endif

//! RAII for GNU C, used for when arrays become too big for the stack.
__attribute__((used)) static inline void dfree(double **p) { if (*p != NULL) free(*p); }
#define _cleanup_free_ __attribute__((cleanup(dfree)))

#if DATA_ARR_LEN > 100000
#define DECL_DARR(x) _cleanup_free_ double* x = (double*) calloc(DATA_ARR_LEN, sizeof(double))
#else
#define DECL_DARR(x) double x[DATA_ARR_LEN]
#endif

//! Step size for calculating exponential between bounds
const double STEP_SIZE = ((X_END - X_START) / ((double) DATA_ARR_LEN));

static void compare_exp_vals(void)
{
    DECL_DARR(exp_m_dat);
    DECL_DARR(exp_s99_dat);

    double smape_top = 0;
    double smape_bottom = 0;
    
    double x_val = X_START;
    for (uint64_t i = 0; i < DATA_ARR_LEN; i++)
    {
        exp_m_dat[i] = EXP_M(x_val);
        exp_s99_dat[i] = EXP_S99(x_val);
        smape_top += fabs(exp_s99_dat[i] - exp_m_dat[i]);
        smape_bottom += exp_s99_dat[i] + exp_m_dat[i];
        // printf("x: %f, y: %f, z: %f\n", x_val, exp_m_dat[i], exp_s99_dat[i]);
        x_val += STEP_SIZE;
    }

    // Compute Pearson correlation
    const double p_val = gsl_stats_correlation(exp_m_dat, 1,
                                               exp_s99_dat, 1,
                                               DATA_ARR_LEN);
    printf("----------------------------------\n"
           "Pearson Correlation Value: %.5f\n", p_val);

    // Calculate mean and variance for each exponential data set.
    const double mean_m = gsl_stats_mean(exp_m_dat, 1, DATA_ARR_LEN);
    const double var_m = gsl_stats_variance_m(exp_m_dat, 1, DATA_ARR_LEN, mean_m);
    const double mean_s = gsl_stats_mean(exp_s99_dat, 1, DATA_ARR_LEN);
    const double var_s = gsl_stats_variance_m(exp_s99_dat, 1, DATA_ARR_LEN, mean_s);
    const double t_test = gsl_cdf_gaussian_P(mean_m - mean_s, var_m + var_s);
    printf("t statistic: %.5f\n", t_test);

    // Calculate Symmetric mean absolute percentage error
    printf("SMAPE (3rd version): %.5f\n", (smape_top / smape_bottom) * 100.0);

    puts("----------------------------------");
}

/**
 * Write exponential values as calulated to a binary file for later analysis.
 */
static void write_exp_vals(const char* fname)
{
    // Open file for writing if output is enabled
    FILE* fp = NULL;
    if (fname != NULL)
    {
        fp = fopen(fname, "w");
        assert(fp != NULL);
    }
    
    DECL_DARR(exp_dat);

    double x_val = X_START;
    for (uint64_t i = 0; i < DATA_ARR_LEN; i++)
    {
        // TODO: Add getopt to change this at runtime
        exp_dat[i] = EXP_M(x_val);
        printf("x: %f, y: %f\n", x_val, exp_dat[i]);
        x_val += STEP_SIZE;
    }

    if (fname != NULL)
    {
        fwrite(exp_dat, sizeof(double), DATA_ARR_LEN, fp);
        assert(fclose(fp) == 0);
    }
}

/**
 * Purely for performance analysis.
 */
static void do_exp_m()
{
    DECL_DARR(exp_dat);

    double x_val = X_START;
    for (uint64_t i = 0; i < DATA_ARR_LEN; i++)
    {
        exp_dat[i] = EXP_M(x_val);
        x_val += STEP_SIZE;
    }

    // Prevent array from being optimized out
    x_val = exp_dat[DATA_ARR_LEN-1];
}

/**
 * Purely for performance analysis.
 */
static void do_exp_s99()
{
    DECL_DARR(exp_dat);

    double x_val = X_START;
    for (uint64_t i = 0; i < DATA_ARR_LEN; i++)
    {
        exp_dat[i] = EXP_S99(x_val);
        x_val += STEP_SIZE;
    }

    // Prevent array from being optimized out
    x_val = exp_dat[DATA_ARR_LEN-1];
}

int main(int argc, char* argv[])
{
    if (argc == 1)
    {
        printf("Usage: %s [1|2|3|4] filename\n", argv[0]);
        return EXIT_SUCCESS;
    }

    // Can we get away with memlocking
    bool memlocked = false;
    struct rlimit64 memlock_lim;
    getrlimit64(RLIMIT_MEMLOCK, &memlock_lim);
    if (memlock_lim.rlim_cur > 2 * sizeof(double) * DATA_ARR_LEN)
    {
        mlockall(MCL_FUTURE);
        memlocked = true;
    }

    const int option = atoi(argv[1]);
    switch (option)
    {
    case 1:  // Write to file, if given a filename
        (void) ((argc == 3) ? write_exp_vals(argv[2]) : write_exp_vals(NULL));
        break;
    case 2:  // Do head-to-head comparison
        compare_exp_vals();
        break;
    case 3:  // Only do EXP_M
        do_exp_m();
        break;
    case 4:  // Only do EXP_S99
        do_exp_s99();
        break;
    default:
        printf("Usage: %s [1|2|3|4] filename\n", argv[0]);        
        break;
    }

    if (memlocked)
    {
        munlockall();
    }

    return EXIT_SUCCESS;
}
